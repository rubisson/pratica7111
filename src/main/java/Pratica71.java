import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import utfpr.ct.dainf.if71c.pratica.ClasseExcecao;
import utfpr.ct.dainf.if71c.pratica.Jogador;
import utfpr.ct.dainf.if71c.pratica.JogadorComparator;
import utfpr.ct.dainf.if71c.pratica.Time;

public class Pratica71 {

  public static void main(String[] args) {
    Time time = new Time();
    Scanner sc = new Scanner(System.in); 
    Jogador jog;
    List<Jogador> listJogador = new ArrayList<>();
    int numeroJog,num=0,numero=0;
    String nome;

    System.out.println("Digite o número de jogadores no time: ");
    numeroJog = Integer.parseInt(sc.nextLine());

    do{
        try{
            System.out.println("Digite o número do jogador");
            numero = Integer.parseInt(sc.nextLine());
            if (numero==0){
                break;
            }
            ClasseExcecao test = new ClasseExcecao(numero);
            System.out.println("Digite o nome do jogador");
            nome = sc.nextLine();
            jog = new Jogador(numero, nome);
            listJogador.add(jog);
            num++;
            // ordem descendente de nome e ascendente de número
            //System.out.println(Jogadores.toString());
        }catch(NumberFormatException ex){
            System.out.println(ex.getMessage());
        }finally{
            Collections.sort(listJogador);
            for(Jogador player:listJogador){
                System.out.println(player.toString());
            }
        }       
    }while (true);
    sc.close();
  }
}