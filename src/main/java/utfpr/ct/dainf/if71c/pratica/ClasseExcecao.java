/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if71c.pratica;

/**
 *
 * @author Rubinho
 */
public class ClasseExcecao {
    private int numero;
    public ClasseExcecao(){
        
    }
    public ClasseExcecao(int numero){
        if (numero<0){
            throw new NumberFormatException("Digite um número maior ou igual a zero.");
        }else{
          this.numero = numero;
        }
    }
}
